# Test service: POST: /carservice/capacity/validate
# with body:
# {
#   "car_class": "#{car_class}",
#   "adults": #{pax_count},
#   "luggage": #{luggage_count},
#   "child_seats": #{baby_seats}
# }

Feature: Checking car capacity
  Background:
  # Assert that response code is 204 for LX car class with 1 passenger, 3 luggage and 2 child seats
  
  @check-car-capacity-valid
  Scenario: Check car capacity for valid combination
     When I search LX limo
     Then response body has fields?: car_class in car_node node with values?: LX
     Then response body has fields?: adults in car_node node with values?: 1
     Then response body has fields?: luggage in car_node node with values?: 3
     Then response body has fields?: child_seats in car_node node with values?: 2
     Then response status is 204
	
   


   @check-car-capacity-invalid
  Scenario: Check car capacity for invalid combination
  # Assert that response code is 409 for LX car class with 5 passengers, 3 luggage and 2 child seats
  # and response body includes error message: Maximum number of passengers is 3
  When I search LX limo
  Then response body has fields?: car_class in car_node node with values?: LX
  Then response body has fields?: adults in car_node node with values?: 5
  Then response body has fields?: luggage in car_node node with values?: 3
  Then response body has fields?: child_seats in car_node node with values?: 2
	Then response status is 409


