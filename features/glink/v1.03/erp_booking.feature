Feature: ERP booking

  @erp-create-valid-airport-pu-reservation
  Scenario: Create valid airport pu reservation
    Given I'm logged in as erp:danet
    And I search for rates from jfk to office
    When I create new booking with valid cc
    Then response status is 200
    And response body has field: id in booking node

