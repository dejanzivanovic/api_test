Given(/^I search for rates from (.+) to (.+)$/) do |pu, doff|
  search_request = fixture_for 'SearchRequest'
  pu_location_node = geocode(Geocoder.search_string_for pu)['location'].first
  do_location_node = geocode(Geocoder.search_string_for doff)['location'].first
  @limo_node = LimoNodes.build pu_location_node: pu_location_node, do_location_node: do_location_node
  search_body = search_request.build_customized_request(limo_node: @limo_node)
  process_http_request :post, '/search', body: search_body
end

