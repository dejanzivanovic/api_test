# Add CarClasssCapacity related steps here

# Assert that response code is 204 for LX car class with 1 passenger, 3 luggage and 2 child seats
When (/^I search LX limo$/) do
  search_id = car_class "LX"
  car_request = fixture_for 'CarNodes'
  car_node = @car_node.merge search_id: search_id
  process_http_request :post, '/carservice/capacity/validate', body:car_request(car: car_node)
end
