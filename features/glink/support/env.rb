require 'rspec'
require 'cucumber/rspec/doubles'
ENV['RACK_ENV'] ||= 'test'

require_relative '../../../lib/api_specs_app'
require_relative '../../../lib/glink_connector'
require_relative '../../../lib/helpers/geocoder'
require_relative '../../../lib/helpers/search_helper'
require_relative '../../../lib/helpers/booking_helper'
ApiConnector.logger = ApiSpecsApp.logger

class ApiWorld
  include GlinkConnector
  include SearchHelper
  include BookingHelper
  include Geocoder
  set_settings_from ApiSpecsApp.config

  def self.set_api_version(version)
    ApiSpecsApp.config[:version] = version
    @api_version = version

    require_relative "../#{version}/support/require_fixtures"
    Dir[File.dirname(__FILE__) + "/../step_definitions/*.rb"].each { |file| require file }
  end

  def self.api_version
    @api_version
  end

  def api_version
    self.class.api_version
  end

  def fixture_for(class_name)
    Module.const_get "Glink::#{api_version.upcase.gsub('.', '_')}::#{class_name}"
  end
end

World do
  ApiWorld.new
end
