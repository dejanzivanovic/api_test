# Todo
- Fork project
- Install dependencies
- Run tests
- Implement missing 2 scenarios
- Commit, push and send pull request

# Dependencies
Project requires Ruby >= 2.0.0

## Setup project dependencies after git clone/pull
    $ gem install bundler
    $ bundle install

# Run tests for Glink API v1.03
    $ script/features features/glink/v1.03/`

You should get output similar to:
![Cucumber output](https://bitbucket.org/burmajam/api_test/raw/master/test.png)

Feature that needs to be completed is located @ [features/glink/v1.03/car_class_capacity.feature](https://bitbucket.org/burmajam/api_test/raw/master/features/glink/v1.03/car_class_capacity.feature). Write down steps that will describe what is asked in comment of each scenario. Use existing reusable steps as described in existing working feature @ [features/glink/v1.03/erp_booking.feature](https://bitbucket.org/burmajam/api_test/raw/master/features/glink/v1.03/erp_booking.feature). Missing step implement in [features/glink/v1.03/step_definitions/car_class_capacity_steps.rb](https://bitbucket.org/burmajam/api_test/raw/master/features/glink/v1.03/step_definitions/car_class_capacity_steps.rb)
