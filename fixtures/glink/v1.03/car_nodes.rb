 # Test service: POST: /carservice/capacity/validate
# with body:
# {
#   "car_class": "#{car_class}",
#   "adults": #{pax_count},
#   "luggage": #{luggage_count},
#   "child_seats": #{baby_seats}
# }
# Assert that response code is 204 for LX car class with 1 passenger, 3 luggage and 2 child seats
module CarNodes
def self.build(options={})
    options ||= {}
    search_id = options[:search_id]
    car_class = options[:car_class] || 'LX'
    adults = options[:adults] || 1
    luggage = options[:luggage] || 3
    child_seats = options[:child_seats] || 2
     result = {
        "type" => type,
        "car_class" => car_class,
        "adults" => adults,
        "luggage" => luggage,
        "child_seats" => child_seats
        },
    result['search_id'] = search_id if search_id
    result['car_class'] = car_class if car_class
    result['adults'] = adults if adults
    result['luggage'] = luggage if luggage
    result['child_seats'] = child_seats if child_seats
    result
  end
end