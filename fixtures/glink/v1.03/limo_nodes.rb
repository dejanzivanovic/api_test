module LimoNodes
  def self.build(options={})
    options ||= {}
    search_id = options[:search_id]
    type = options[:type] || "POINT_TO_POINT"
    vehicle_type = options[:vehicle_type] || 'LX'
    adults = options[:adults] || 1
    luggage = options[:luggage] || 0
    schedule_type = options[:schedule_type] || 'LATER'
    pu_time = options[:pu_time] || "09/16/#{Date.today.year + 1} 01:20 AM"
    pu_location = options[:pu_location_node]
    pu_location ||= options.has_key?(:pu_location) ? LocationNodes.send(options[:pu_location].to_sym) : LocationNodes.jfk
    flight_node = options[:flight_node] || LocationNodes.flight_details if search_id && pu_location['category'] == 'Airport'
    do_location = options[:do_location_node]
    do_location ||= options.has_key?(:do_location) ? LocationNodes.send(options[:do_location].to_sym) : LocationNodes.office
    stops = []
    stops << LocationNodes.send(options[:stop]) if options.has_key?(:stop)
    customer_comments = options[:customer_comments] || ''
    promo_code = options[:promo_code] || 'RIDE4FREE'
    result = {
        "type" => type,
        "vehicle_type" => vehicle_type,
        "adults" => adults,
        "luggage" => luggage,
        "schedule" => {
            "type" => schedule_type,
            "value" => pu_time
        },
        "pickup" => {
            "location" => pu_location,
            "user_address" => pu_location['name']
        },
        "destination" => {
            "location" => do_location,
            "user_address" => do_location['name']
        },
        "stops" => stops,
        "customer_comments" => customer_comments
    }
    result['search_id'] = search_id if search_id
    result['coupon'] = promo_code if promo_code
    result['pickup']['flight'] = flight_node if flight_node
    result
  end
end
