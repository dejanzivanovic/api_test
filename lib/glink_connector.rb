require_relative 'api_connector' unless defined? ApiConnector

module GlinkConnector
  def self.included(base)
    base.instance_eval {
      include InstanceMethods
      include ApiConnector
    }
    base.extend ClassMethods
  end

  module InstanceMethods
    def inject_headers!(headers, options={})
      headers['Authorization'] = "OAuth2 #{self.class.access_token}" unless options[:skip_authorization] == true
    end

    def authorize(role, username, password=nil)
      self.class.authorize(role, username, password)
    end
  end

  module ClassMethods
    def target_api
      :glink
    end

    def access_token
      @access_token
    end

    def check_api_availability
      authorize.code == 200
    end

    def authorize(role, username, password=nil)
      headers = {
          'Content-Type' => 'application/json',
          'Accept' => 'application/json'
      }
      query_username = "#{role}:#{username}"
      query_password = password || settings[:valid_credentials]["#{role}_#{username}".to_sym]
      options = settings[:authentication].merge({username: query_username, password: query_password, grant_type: 'password'})
      url = url_for('/oauth/authorize')
      ApiConnector.logger.info "GET: #{url}"
      ApiConnector.logger.debug "Request Headers: #{headers}"
      ApiConnector.logger.debug "Request Query: #{options}"
      @response = response = get url, query: options
      @user_id = response.parsed_response['user_id']
      @access_token = response.parsed_response['access_token']
      ApiConnector.logger.info "GL responded with #{@response.code}"
      ApiConnector.logger.debug "Response Body:\n#{@response.parsed_response.to_json}"
      @response
    rescue Timeout::Error, Net::HTTPServerException, Errno::EHOSTUNREACH, Errno::ECONNREFUSED, Errno::ENETUNREACH, Errno::ETIMEDOUT => e
      raise ApiConnector::ApiUnavailable.new e.message
    end

  end

  class << self
    attr_reader :user_id

    def check_api_availability
      x = Class.new.instance_eval { include GlinkConnector }
      x.check_api_availability
    end
  end
end
